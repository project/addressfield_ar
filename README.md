# Addressfield Argentina

This module is a plugin for [Addressfield][0] that provides individual fields
for various address components.

When enabled, the address form will collect

* Street name
* Street number
* Block / Building
* Floor
* Apartment / Office
* County

The regular _Address 1_ and _Address 2_ fields will be automatically filled
based on the other values.

It was developed to use together with [Commerce Etrans][1] and
[Commerce Urbano][2], two shipping services from Argentina.

## Installation

Just download and enabled the module. 

## Usage

Enable _Address form (specific for Argentina)_ in the field settings.



[0]: https://www.drupal.org/project/addressfield     "Addressfield"
[1]: https://www.drupal.org/project/commerce_etrans  "Commerce Etrans"
[1]: https://www.drupal.org/project/commerce_urbano  "Commerce Urbano"
